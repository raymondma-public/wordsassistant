package com.ma.raymond.learnnewwords;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heihe on 12/8/2018.
 */

class LexicalEntry {
    private List<Sense> senses = new ArrayList();
    private String lexicalCategory = "";

    public void addSense(Sense sense) {
        senses.add(sense);
    }

    public List<Sense> getSenses() {
        return senses;
    }


    public String getLexicalCategory() {
        return lexicalCategory;
    }

    public void setLexicalCategory(String lexicalCategory) {
        this.lexicalCategory = lexicalCategory;
    }
}
