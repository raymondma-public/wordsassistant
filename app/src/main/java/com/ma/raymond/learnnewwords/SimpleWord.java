package com.ma.raymond.learnnewwords;

/**
 * Created by heihe on 12/8/2018.
 */

public class SimpleWord {
    private String word = "";
    private String description = "";

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SimpleWord(String word, String description) {

        this.word = word;
        this.description = description;
    }
}
