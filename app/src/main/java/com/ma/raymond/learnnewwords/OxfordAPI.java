package com.ma.raymond.learnnewwords;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by heihe on 12/8/2018.
 */


//in android calling network requests on the main thread forbidden by default
//create class to do async job
public class OxfordAPI extends AsyncTask<String, Integer, String> {

    MainActivity mainActivity;

    public OxfordAPI(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected String doInBackground(String... params) {

        //TODO: replace with your own app id and app key
        final String app_id = Constant.APPLICATION_ID;
        final String app_key = Constant.APPLICATION_KEYS;
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("app_id", app_id);
            urlConnection.setRequestProperty("app_key", app_key);

            // read the output from the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

//        System.out.println(result);
        Log.d("OxsordAPI", result);


        try {
            JSONObject responseJson = new JSONObject(result);
            JSONArray resultJsons = responseJson.getJSONArray("results");
            Word word = null;
            for (int resultJsonIdx = 0; resultJsonIdx < resultJsons.length(); resultJsonIdx++) {
                JSONObject resultJson = resultJsons.getJSONObject(resultJsonIdx);
                JSONArray lexicalEntrieJsons = resultJson.getJSONArray("lexicalEntries");
                if (resultJsonIdx == 0) {
                    String queryWord = resultJson.getString("word");
                    word = new Word(queryWord);
                }
                for (int lexicalEntrieJsonIdx = 0; lexicalEntrieJsonIdx < lexicalEntrieJsons.length(); lexicalEntrieJsonIdx++) {
                    JSONObject lexicalEntrieJson = lexicalEntrieJsons.getJSONObject(lexicalEntrieJsonIdx);
                    String lexicalCategory = lexicalEntrieJson.getString("lexicalCategory");
                    JSONArray entrieJsons = lexicalEntrieJson.getJSONArray("entries");

                    LexicalEntry lexicalEntry = new LexicalEntry();
                    lexicalEntry.setLexicalCategory(lexicalCategory);
                    for (int entrieJsonIdx = 0; entrieJsonIdx < entrieJsons.length(); entrieJsonIdx++) {
                        JSONObject entryJson = entrieJsons.getJSONObject(entrieJsonIdx);
                        JSONArray senseJsons = entryJson.getJSONArray("senses");


                        for (int sensesIdx = 0; sensesIdx < senseJsons.length(); sensesIdx++) {
                            JSONObject senseJson = senseJsons.getJSONObject(sensesIdx);

                            Sense sense = new Sense();
                            try {
                                JSONArray exampleJsons = senseJson.getJSONArray("examples");
                                for (int exampleJsonIdx = 0; exampleJsonIdx < exampleJsons.length(); exampleJsonIdx++) {
                                    JSONObject exampleJson = exampleJsons.getJSONObject(exampleJsonIdx);
                                    String exampleText = exampleJson.getString("text");
                                    sense.addExample(exampleText);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            try {
                                JSONArray synonymJsons = senseJson.getJSONArray("synonyms");
                                for (int i = 0; i < synonymJsons.length(); i++) {
                                    JSONObject synonymJson = synonymJsons.getJSONObject(i);
                                    String synonymText = synonymJson.getString("text");
                                    sense.addSynonym(synonymText);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            lexicalEntry.addSense(sense);
                        }

                    }
                    word.addLexicalEntry(lexicalEntry);
                }
            }


            Handler handler = mainActivity.getHandler();
            Message message = new Message();
            message.what = 1;


            message.obj = word;
            mainActivity.getHandler().sendMessage(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}

