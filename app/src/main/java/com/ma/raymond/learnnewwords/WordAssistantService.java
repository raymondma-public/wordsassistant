package com.ma.raymond.learnnewwords;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by heihe on 11/8/2018.
 */

public class WordAssistantService extends Service {

    private ScreenObserver screenObserver;
    private static String TAG = "WordAssistantService";

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    public class LocalBinder extends Binder {
        public WordAssistantService getService() {
            // Return this instance of LocalService so clients can call public methods
            return WordAssistantService.this;
        }
    }


    @Override
    public void onStart(Intent intent, int startId) {
        Log.i("time:", "onStart");
        super.onStart(intent, startId);

        if (screenObserver == null) {
            //Parse SDK stuff goes here
            screenObserver = new ScreenObserver(getApplicationContext());
            screenObserver.startObserver(new ScreenObserver.ScreenStateListener() {
                @Override
                public void onScreenOn() {
                    Log.d(TAG, "onScreenOn");


                }

                @Override
                public void onScreenOff() {
                    Log.d(TAG, "onScreenOff");
                }

                @Override
                public void onUserPresent() {
                    Log.d(TAG, "onUserPresent");
                    Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(dialogIntent);
//
//                    Intent intent = new Intent(getApplicationContext(), OverlayShowingService.class);
//                    startService(intent);
//                    Intent dialogIntent = new Intent(getApplicationContext(), MainActivity.class);
//                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(dialogIntent);
                }
            });
        }


    }


}
