package com.ma.raymond.learnnewwords;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heihe on 12/8/2018.
 */

class Sense {
    private List<String> synonyms = new ArrayList<>();
    private List<String> examples = new ArrayList<>();

    public void addSynonym(String value) {
        synonyms.add(value);
    }

    public void addExample(String value) {
        examples.add(value);
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public List<String> getExamples() {
        return examples;
    }
}
