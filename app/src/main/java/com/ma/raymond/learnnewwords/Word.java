package com.ma.raymond.learnnewwords;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heihe on 12/8/2018.
 */

public class Word {
    private String word = "";
    private List<LexicalEntry> lexicalEntries = new ArrayList();

    public Word(String word) {
        this.word = word;
    }

    public void addLexicalEntry(LexicalEntry lexicalEntry) {
        this.lexicalEntries.add(lexicalEntry);

    }

    public String getWord() {
        return word;
    }


    public List<LexicalEntry> getLexicalEntries() {
        return lexicalEntries;
    }

    public void setLexicalEntries(List<LexicalEntry> lexicalEntries) {
        this.lexicalEntries = lexicalEntries;
    }
}
