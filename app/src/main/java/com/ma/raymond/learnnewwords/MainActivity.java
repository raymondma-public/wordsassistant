package com.ma.raymond.learnnewwords;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.knziha.plod.dictionary.mdict;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    private String TAG = "MainActivity";
    private boolean mBound = false;
    WordAssistantService wordAssistantService;
    TextView wordTV;

    TextView titleTV;
    TextView descriptionTV;
    Switch unlockSwitch;


    //Text to speech
    TextToSpeech tts;

    SimpleWord currentWord = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "MainActivity oncreate");
        Intent intent = new Intent(MainActivity.this, WordAssistantService.class);
        startService(intent);
        wordTV = (TextView) findViewById(R.id.wordTV);
        titleTV = (TextView) findViewById(R.id.titleTV);
        unlockSwitch = (Switch) findViewById(R.id.unlockSwitch);
        descriptionTV = (TextView) findViewById(R.id.descriptionTV);
        unlockSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    finish();
                }
            }
        });


        refreshRandomWord();

//        String key = "apple";
////        String path = "/storage/emulated/0/Dictionary/Oxford8.mdd";
//        File sdcard = Environment.getExternalStorageDirectory();
//        String path = sdcard.getPath()+"/Dictionary/Oxford8.mdd";
//
//        File file=new File(path);
//        Log.d(TAG, "file.getAbsolutePath()=" + file.getAbsolutePath());
//        Log.d(TAG, "file.getName()=" + file.getName());
//        Log.d(TAG, "file.exists()=" + file.exists());
//        Log.d(TAG, "file.getTotalSpace()=" + file.getTotalSpace());
//        DataInputStream data_in=null;
//        try {
//            data_in =new DataInputStream(new FileInputStream(file));
//            int read=data_in.read();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if(data_in!=null){
//                try {
//                    data_in.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//
//        try {
//            mdict md = new com.knziha.plod.dictionary.mdict(path);
//            int search_result = 0;
//            search_result = md.lookUp(key);
//
//            Log.d(TAG, "search_result=" + search_result);
//            if (search_result != -1) {
//                String html_contents = md.getRecordAt(search_result);
//                String entry_name_at_pos = md.getEntryAt(search_result);
//                //TODO handle html_contents and entry_name_at_pos
//                Log.d(TAG, "html_contents=" + html_contents);
//                Log.d(TAG, "entry_name_at_pos=" + entry_name_at_pos);
//
//            }
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    private void refreshRandomWord() {
        SimpleWord randomWord = getRandomWordFromList();
        if (randomWord == null) {
            randomWord = new SimpleWord("error", "error");
        }

        currentWord = randomWord;
        titleTV.setText(randomWord.getWord());
        descriptionTV.setText(randomWord.getDescription());
        wordTV.setText("loading...");
        queryOxford(randomWord.getWord().replace("*", ""));

    }

    private SimpleWord getRandomWordFromList() {
        AssetManager am = this.getAssets();
        InputStream is = null;
        BufferedReader reader = null;
        InputStreamReader isr = null;
        try {
            is = am.open("ielts_word_list.txt");
            isr = new InputStreamReader(is);
            reader = new BufferedReader(isr);
            StringBuilder out = new StringBuilder();
            String line;
            int i = 0;
            int wordIdx = (new Random()).nextInt(countLines("ielts_word_list.txt"));
            while ((line = reader.readLine()) != null) {
                Log.d("MainActivity", i + " " + wordIdx + " " + line);
                if (i == wordIdx) {

                    String lines[] = line.split(" ", 2);

                    return new SimpleWord(lines[0], lines[1]);
                }
                i++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public int countLines(String filename) throws IOException {
        int count = 0;
        AssetManager am = this.getAssets();
        InputStream is = null;
        BufferedReader reader = null;
        InputStreamReader isr = null;
        try {
            is = am.open("ielts_word_list.txt");
            byte[] c = new byte[1024];

            int readChars = 0;
            boolean empty = true;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            return (count == 0 && !empty) ? 1 : count;
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return count;

    }

    private void queryOxford(String queryWord) {
        new OxfordAPI(this).execute(dictionaryEntries(queryWord));
    }

    public Handler getHandler() {
        return mHandler;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Word word = (Word) msg.obj;
                    String displyString = "";
                    for (LexicalEntry lexicalEntry : word.getLexicalEntries()) {
                        displyString += "\nLexical Category: " + lexicalEntry.getLexicalCategory() + "\n";
                        for (int i = 0; i < lexicalEntry.getSenses().size(); i++) {
                            Sense sense = lexicalEntry.getSenses().get(i);
                            displyString += "\nSense" + i + "===========================: \n";
                            displyString += "\nExamples: \n\n";
                            for (int j = 0; j < sense.getExamples().size(); j++) {
                                String example = sense.getExamples().get(j);
                                displyString += j + ". " + example + "\n";
                            }

                            displyString += "\nSynonyms: \n\n";
                            for (int j = 0; j < sense.getSynonyms().size(); j++) {
                                String synonyms = sense.getSynonyms().get(j);
                                displyString += j + ". " + synonyms + "\n";
                            }
                        }
                    }

                    wordTV.setText(displyString);
                    titleTV.setText(word.getWord());
                    break;
            }
        }
    };

    private String dictionaryEntries(String queryWord) {
        final String language = "en";
        final String word = queryWord;
        final String word_id = word.toLowerCase(); //word id is case sensitive and lowercase is required
        return "https://od-api.oxforddictionaries.com:443/api/v1/entries/" + language + "/" + word_id + "/synonyms;antonyms";
    }


    private static final int PERMISSION_LOCATION_REQUEST_CODE = 1;

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Settings.ACTION_MANAGE_OVERLAY_PERMISSION) == PackageManager.PERMISSION_GRANTED
                ;
    }

    private void showPermissionDialog() {
        if (!checkPermission(this)) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Settings.ACTION_MANAGE_OVERLAY_PERMISSION}, PERMISSION_LOCATION_REQUEST_CODE
            );
        }
    }

    public void onStart() {
        super.onStart();

        showPermissionDialog();
        //welle
        if (wordAssistantService == null) {
            Intent intent = new Intent(this, WordAssistantService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Welle Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }


    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            WordAssistantService.LocalBinder binder = (WordAssistantService.LocalBinder) service;
            wordAssistantService = binder.getService();
            if (wordAssistantService != null) {
                Log.d(TAG, "wordAssistantService callback set");
            } else {
                Log.d(TAG, "wordAssistantService callback not set");
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public void nextRandomWord(View view) {
        refreshRandomWord();
    }

    public void speakCurrent(View view) {
        speak(currentWord.getWord().replace("*", ""));
    }

    private void speak(String messageToSpeak) {
        tts.speak(messageToSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }
}
